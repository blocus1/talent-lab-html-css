<?php

return [
    "home" => 'Mon blog',
    "catalog" => 'Nos articles',
    "contact" => 'Contactez nous',
    "login" => 'Se connecter',
];
