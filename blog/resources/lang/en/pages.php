<?php

return [
    "home" => 'My blog',
    "catalog" => 'Articles',
    "contact" => 'Contact us',
    "login" => 'Login',
    "register" => 'Register',
    "hello" => 'Hello'
];
