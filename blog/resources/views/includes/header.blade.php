<?php
    $locals = ['ar', 'fr', 'en'];
?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <div class="container">

  <a class="navbar-brand" href="{{ route('index') }}">@lang("pages.home")</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('article.index') }}">@lang("pages.catalog") <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('contact') }}">@lang("pages.contact") <span class="sr-only">(current)</span></a>
      </li>
    </ul>

    <ul class="navbar-nav ">
        @foreach($locals as $l)
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('changeLang', $l) }}">{{$l}}</a>
        </li>
        @endforeach

        @if(Auth::user())
        <li class="nav-item active">
            <span class="nav-link">@lang('pages.hello') {{Auth::user()->name}}</span>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
            </li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
        @else
        <a class="nav-link" href="{{ route('login') }}">@lang('pages.login')</a>
        <a class="nav-link" href="{{ route('register') }}">@lang('pages.register')</a>

        @endif


    </ul>
  </div>
  </div>
</nav>
