@extends('layouts.public')
@section('content')
<div class="album py-5 bg-light">
  <div class="container">
      <h1>Modifier un article</h1>
      <form action="{{route('article.update', $article->id)}}" method="POST">
          @method('PATCH')
          @csrf
        <div class="form-group">
          <label for="exampleInputEmail1">Titre</label>
          <input type="text" class="form-control" id="titre" name="titre"
            value="{{$article->titre}}">
        </div>
        @error('titre')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="exampleInputPassword1">Lien image de couverture</label>
          <input type="text" class="form-control" id="img_url" name="img_url"
            value="{{$article->img_url}}" >
            @error('img_url')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <textarea
                class="form-control"
                id="body"
                name="body"
                rows="5"
            >{{$article->body}}</textarea>
            @error('body')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Modifier</button>
      </form>
  </div>
</div>


@endsection
