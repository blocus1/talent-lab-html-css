@extends('layouts.public')
@section('content')
<div class="album py-5 bg-light">
  <div class="container">
      @if (Auth::user())
        <a href="{{route('article.create')}}" class="btn btn-success">
            Ajouter un article
        </a>
      @endif


      {{$articles->links()}}
    <div class="row">
        @foreach ($articles as $a)
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card mb-4 shadow-sm">
                    <img
                        src="{{$a->img_url}}"
                        class="bd-placeholder-img card-img-top"
                    />
                    <div class="card-body">
                        <h5 class="card-title">{{ $a->titre }}</h5>
                        <a href="{{ route('article.show',$a->id) }}" class="btn btn-dark">
                            Lire l'article
                        </a>
                    </div>
                </div>
            </div>
      @endforeach
    </div>
    {{$articles->links()}}
  </div>
</div>


@endsection
