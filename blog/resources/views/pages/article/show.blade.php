@extends('layouts.public')
@section('content')
<div class="album py-5 bg-light">
    <div class="container">
        <h1>{{$article->titre}}</h1>
        <hr />
        {{$article->auteur->name}}
        {{$article->created_at}}

        @if($article->created_at != $article->updated_at)
        <i>Article modifié le ({{$article->updated_at}})</i>
        @endif

        @if($article->user_id == Auth::user()->id)
            <a href="{{route('article.edit', $article->id)}}">Modifier</a>
            <form
                id="delete-form"
                class="d-inline-block"
                action="{{route('article.destroy', $article->id)}}"
                method="POST"
            >
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-link">Supprimer</button>

            </form>

        @endif

        <hr />
        <img src="{{$article->img_url}}" class="w-100"  />
        <div>
            @foreach ($article->parsed_body as $line)
                <p>{{$line}}</p>
            @endforeach
        </div>
    </div>
</div>



@endsection


@section('js')
<script>
    $('#delete-form').submit(function(e){
        e.preventDefault();
        if( confirm("Êtes vous sur de supprimer cet article")){
            e.currentTarget.submit();
        }
    });
</script>
@endsection
