<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'titre',
        'body',
        'img_url',
        'user_id',
    ];

    public function auteur()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
