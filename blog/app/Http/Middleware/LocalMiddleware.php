<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;

class LocalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $local = 'fr';
        if (Auth::user()) {
            $local = Auth::user()->local;
        } elseif ($request->cookie('local')) {
            $local = $request->cookie('local');
        }

        App::setlocale($local);
        return $next($request);

        //
    }
}
