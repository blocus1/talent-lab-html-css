<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $response = new Response('Hello World');
        // $response->withCookie(cookie('local', 'ar', 525600));
        // return $response;

        $articles = Article::orderBy('created_at', "desc")->paginate(12);
        return view('pages.article.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user) {
            return view('pages.article.create');
        } else {
            return redirect('/login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titre' => 'required|max:255',
            'body' => 'required',
            'img_url' => "url|nullable",
        ]);

        $article = Article::create([
            'titre' => $request->titre,
            'body' => $request->body,
            'img_url' => $request->img_url,
            'user_id' => Auth::user()->id,
        ]);

        return redirect(route("article.show", $article->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $article->parsed_body = explode("\n", $article->body);
        return view('pages.article.show', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        // return $article;
        $user = Auth::user();
        if ($user) {
            if ($user->id == $article->user_id) {
                return view('pages.article.edit', ['article' => $article]);
            } else {
                return redirect(route('article.index'));
            }
        } else {
            return redirect('/login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $user = Auth::user();
        if ($user) {
            if ($user->id == $article->user_id) {
                $request->validate([
                    'titre' => 'required|max:255',
                    'body' => 'required',
                    'img_url' => "url|nullable",
                ]);

                $article->titre = $request->titre;
                $article->body = $request->body;
                $article->img_url = $request->img_url;
                $article->save();

                return redirect(route('article.show', $article->id));
            } else {
                return redirect(route('article.index'));
            }
        } else {
            return redirect('/login');
        }

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $user = Auth::user();
        if ($user) {
            if ($user->id == $article->user_id) {
                $article->delete();
            }
            return redirect(route('article.index'));
        } else {
            return redirect('/login');
        }
        return 'DELETING ' . $article->id;
    }
}
