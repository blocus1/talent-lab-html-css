<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class cookiesController extends Controller
{
    public function setCookie(Request $request, $local)
    {
        if (Auth::user()) {
            Auth::user()->local =  $local;
            Auth::user()->save();
        }


        $minutes = 60*24*365;

        return back()->withCookie(cookie('local', $local, $minutes));
    }
}
