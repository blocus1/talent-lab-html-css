<?php


use App\Article;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'titre' => $faker->sentence(),
        'body' => $faker->paragraphs(5, true),
        'user_id' => $faker->numberBetween(1, 100),
    ];
});
