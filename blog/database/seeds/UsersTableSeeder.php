<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 100)->create();

        // DB::table('users')->insert([
        //     'name' => 'Ahmed',
        //     'email' => 'contact@ahmedmeftah.com',
        //     'password' => bcrypt("azeazeaze"),
        // ]);
        //
        // DB::table('users')->insert([
        //     'name' => 'Nada',
        //     'email' => 'nada@gmail.com',
        //     'password' => bcrypt("azeazeaze"),
        // ]);
        //
        // DB::table('users')->insert([
        //     'name' => 'Hend',
        //     'email' => 'hend@gmail.com',
        //     'password' => bcrypt("azeazeaze"),
        // ]);
    }
}
