<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('pages.index');
})->name('index');

Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');

Route::get('/changeLang/{local}', "cookiesController@setCookie")->name('changeLang');


Route::get('/catalogue', function () {
    return view('pages.catalog');
})->name('catalog');

// Route::get('/article', function () {
//     return view('pages.article');
// })->name('article');

Route::resource('article', 'ArticleController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
